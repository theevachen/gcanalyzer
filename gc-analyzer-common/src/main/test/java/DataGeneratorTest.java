import com.yammer.coreservices.gc.DataEvent;
import com.yammer.coreservices.gc.GCLogDataEnum;
import org.assertj.core.data.Offset;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by echen on 8/18/14.
 */
public class DataGeneratorTest {
    String testFileName = "TestInput";

    @Test
    public void generateDataTest() throws IOException {
        DataEvent parNewEvents = new DataEvent(testFileName);

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.timesbetweengarbagecollections)).isEqualTo(2);
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.timesbetweengarbagecollections)).isEqualTo(2);
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.timesbetweengarbagecollections)).isEqualTo(1);

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.memoryallocated)).isEqualTo(0.035, Offset.offset(.0001));
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.memoryallocated)).isEqualTo(0.045, Offset.offset(.0001));
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.memoryallocated)).isEqualTo(0.06, Offset.offset(.0001));

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.frequencies)).isEqualTo(.5);
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.frequencies)).isEqualTo(.5);
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.frequencies)).isEqualTo(1.0);

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.younggenmemorycleared)).isEqualTo(0.01, Offset.offset(.0001));
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.younggenmemorycleared)).isEqualTo(0.02, Offset.offset(.0001));
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.younggenmemorycleared)).isEqualTo(0.05, Offset.offset(.0001));
        assertThat(parNewEvents.get(3).getMap().get(GCLogDataEnum.younggenmemorycleared)).isEqualTo(0.02, Offset.offset(.0001));

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.oldgenbeforegc)).isEqualTo(0.005, Offset.offset(.0001));
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.oldgenbeforegc)).isEqualTo(-0.015, Offset.offset(.0001));
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.oldgenbeforegc)).isEqualTo(-0.035, Offset.offset(.0001));
        assertThat(parNewEvents.get(3).getMap().get(GCLogDataEnum.oldgenbeforegc)).isEqualTo(-0.042, Offset.offset(.0001));

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.oldgenaftergc)).isEqualTo(0.02, Offset.offset(.0001));
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.oldgenaftergc)).isEqualTo(0.007, Offset.offset(.0001));
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.oldgenaftergc)).isEqualTo(0.02, Offset.offset(.0001));
        assertThat(parNewEvents.get(3).getMap().get(GCLogDataEnum.oldgenaftergc)).isEqualTo(-0.017, Offset.offset(.0001));

        assertThat(parNewEvents.get(0).getMap().get(GCLogDataEnum.memorypromoted)).isEqualTo(0.015, Offset.offset(.0001));
        assertThat(parNewEvents.get(1).getMap().get(GCLogDataEnum.memorypromoted)).isEqualTo(0.022, Offset.offset(.0001));
        assertThat(parNewEvents.get(2).getMap().get(GCLogDataEnum.memorypromoted)).isEqualTo(0.055, Offset.offset(.0001));
        assertThat(parNewEvents.get(3).getMap().get(GCLogDataEnum.memorypromoted)).isEqualTo(0.025, Offset.offset(.0001));

    }
}
