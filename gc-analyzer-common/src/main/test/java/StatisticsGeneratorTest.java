import com.yammer.coreservices.gc.DataEvent;
import com.yammer.coreservices.gc.GCLogDataEnum;
import com.yammer.coreservices.gc.Statistic;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by echen on 8/18/14.
 */
public class StatisticsGeneratorTest {
    String testFileName = "TestInput";

    @Test
    public void generateStatisticsTest() throws IOException {
        DataEvent parNewEvents = new DataEvent(testFileName);
        Map<GCLogDataEnum, Statistic> statisticsMap = parNewEvents.generateStatistics();

        Statistic statistic = new Statistic(5 / 3.0, 2.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 5.0);
        assertThat(statisticsMap.get(GCLogDataEnum.timesbetweengarbagecollections)).isEqualTo(statistic);

        statistic = new Statistic(140 / 3000.0, 60.0 / 1000, 35.0 / 1000, 45.0 / 1000, 60.0 / 1000, 60.0 / 1000, 60.0 / 1000, 60.0 / 1000, 140.0 / 1000);
        assertThat(statisticsMap.get(GCLogDataEnum.memoryallocated)).isEqualTo(statistic);

        statistic = new Statistic(2 / 3.0, 1.0, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 2.0);
        assertThat(statisticsMap.get(GCLogDataEnum.frequencies)).isEqualTo(statistic);

        statistic = new Statistic(0.025, 0.05, 0.01, 0.02, 0.05, 0.05, 0.05, 0.05, 0.1);
        assertThat(statisticsMap.get(GCLogDataEnum.younggenmemorycleared)).isEqualTo(statistic);

        statistic = new Statistic(-21.75 / 1000, 5.0 / 1000, -42.0 / 1000, -35.0 / 1000, 5.0 / 1000, 5.0 / 1000, 5.0 / 1000, 5.0 / 1000, -87.0 / 1000);
        assertThat(statisticsMap.get(GCLogDataEnum.oldgenbeforegc)).isEqualTo(statistic);

        statistic = new Statistic(7.5 / 1000, 20.0 / 1000, -17.0 / 1000, 7.0 / 1000, 20.0 / 1000, 20.0 / 1000, 20.0 / 1000, 20.0 / 1000, 30.0 / 1000);
        assertThat(statisticsMap.get(GCLogDataEnum.oldgenaftergc)).isEqualTo(statistic);

        statistic = new Statistic(29.25 / 1000, 55.0 / 1000, 15.0 / 1000, 22.0 / 1000, 55.0 / 1000, 55.0 / 1000, 55.0 / 1000, 55.0 / 1000, 117.0 / 1000);
        assertThat(statisticsMap.get(GCLogDataEnum.memorypromoted)).isEqualTo(statistic);
    }
}
