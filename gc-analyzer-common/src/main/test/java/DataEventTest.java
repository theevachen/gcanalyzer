import com.yammer.coreservices.gc.DataEvent;
import com.yammer.coreservices.gc.GCLogDataEnum;
import com.yammer.coreservices.gc.ParNewEvent;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by echen on 8/18/14.
 */
public class DataEventTest {

    String testFileName = "TestInput";

    @Test
    public void readFileTest() throws IOException {

        List<ParNewEvent> parNewEvents = new ArrayList<>();
        Map<GCLogDataEnum, Double> map = new HashMap<>();

        map.put(GCLogDataEnum.timestamps, 80.000);
        map.put(GCLogDataEnum.durations, 0.500);
        map.put(GCLogDataEnum.younggenoccupanciesbeforegc, 0.1);
        map.put(GCLogDataEnum.younggenoccupanciesaftergc, 0.09);
        map.put(GCLogDataEnum.heapoccupanciesbeforegc, 0.105);
        map.put(GCLogDataEnum.heapoccupanciesaftergc, 0.110);
        map.put(GCLogDataEnum.cpuusertimes, 0.200);
        map.put(GCLogDataEnum.cpusystimes, 0.000);
        map.put(GCLogDataEnum.realtimes, 0.100);
        ParNewEvent parNewEvent = new ParNewEvent(map);
        parNewEvents.add(parNewEvent);

        map = new HashMap<>();
        map.put(GCLogDataEnum.timestamps, 82.000);
        map.put(GCLogDataEnum.durations, 0.100);
        map.put(GCLogDataEnum.younggenoccupanciesbeforegc, 0.125);
        map.put(GCLogDataEnum.younggenoccupanciesaftergc, 0.105);
        map.put(GCLogDataEnum.heapoccupanciesbeforegc, 0.11);
        map.put(GCLogDataEnum.heapoccupanciesaftergc, 0.112);
        map.put(GCLogDataEnum.cpuusertimes, 0.700);
        map.put(GCLogDataEnum.cpusystimes, 0.100);
        map.put(GCLogDataEnum.realtimes, 0.600);
        parNewEvent = new ParNewEvent(map);
        parNewEvents.add(parNewEvent);

        map = new HashMap<>();
        map.put(GCLogDataEnum.timestamps, 84.000);
        map.put(GCLogDataEnum.durations, 0.200);
        map.put(GCLogDataEnum.younggenoccupanciesbeforegc, 0.15);
        map.put(GCLogDataEnum.younggenoccupanciesaftergc, 0.1);
        map.put(GCLogDataEnum.heapoccupanciesbeforegc, 0.115);
        map.put(GCLogDataEnum.heapoccupanciesaftergc, 0.12);
        map.put(GCLogDataEnum.cpuusertimes, 0.300);
        map.put(GCLogDataEnum.cpusystimes, 0.200);
        map.put(GCLogDataEnum.realtimes, 0.500);
        parNewEvent = new ParNewEvent(map);
        parNewEvents.add(parNewEvent);

        map = new HashMap<>();
        map.put(GCLogDataEnum.timestamps, 85.000);
        map.put(GCLogDataEnum.durations, 0.300);
        map.put(GCLogDataEnum.younggenoccupanciesbeforegc, 0.16);
        map.put(GCLogDataEnum.younggenoccupanciesaftergc, 0.14);
        map.put(GCLogDataEnum.heapoccupanciesbeforegc, 0.118);
        map.put(GCLogDataEnum.heapoccupanciesaftergc, 0.123);
        map.put(GCLogDataEnum.cpuusertimes, 0.200);
        map.put(GCLogDataEnum.cpusystimes, 0.300);
        map.put(GCLogDataEnum.realtimes, 0.400);
        parNewEvent = new ParNewEvent(map);
        parNewEvents.add(parNewEvent);

        assertThat(new DataEvent(testFileName).getParNewEvents()).isEqualTo(parNewEvents);
    }
}
