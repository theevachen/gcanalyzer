import com.yammer.coreservices.gc.GCLogDataEnum;
import com.yammer.coreservices.gc.Options;
import com.yammer.coreservices.gc.OptionsParser;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by echen on 8/18/14.
 */

public class OptionsParserTest {

    @Test
    public void createOptions() {
        OptionsParser optionsParser = new OptionsParser();
        String[] args = {"-timestamps", "true", "file", "TestInput.log"};

        List<GCLogDataEnum> enums = new ArrayList<>();
        enums.add(GCLogDataEnum.timestamps);

        Options options = optionsParser.parseOptions(args);

        assertThat(options.getFileName()).isEqualTo("TestInput.log");
        assertThat(options.getSummary()).isEqualTo(true);
        assertThat(options.getEnums()).isEqualTo(enums);
    }

}
