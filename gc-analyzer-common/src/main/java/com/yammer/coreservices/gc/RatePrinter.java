package com.yammer.coreservices.gc;

/**
 * Created by echen on 8/28/14.
 */
public class RatePrinter {
    public void printRate(RateEnum enums, Rate rate, String format, UnitProvider unitProvider) {

        String leftAlignFormat = format;
        System.out.format(leftAlignFormat, enums.name(), unitProvider.getRatesUnitMap().get(enums), rate.getMap().get(enums));

    }
}
