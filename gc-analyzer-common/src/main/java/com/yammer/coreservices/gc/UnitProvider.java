package com.yammer.coreservices.gc;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by echen on 8/25/14.
 */
public class UnitProvider {
    Map<GCLogDataEnum, Unit> statsUnitMap;
    Map<RateEnum, Unit> ratesUnitMap;

    public UnitProvider() {
        this.statsUnitMap = buildStatsUnitMap();
        this.ratesUnitMap = buildRatesUnitMap();
    }

    public Map<RateEnum, Unit> getRatesUnitMap() {
        return ratesUnitMap;
    }

    public Map<GCLogDataEnum, Unit> getStatsUnitMap() {
        return statsUnitMap;
    }

    private Map<RateEnum, Unit> buildRatesUnitMap() {
        Map<RateEnum, Unit> ratesUnitMap = new HashMap<>();

        ratesUnitMap.put(RateEnum.allocPerUserSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.allocPerSysSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.allocPerRealSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.allocPerTotalSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.promotedPerUserSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.promotedPerSysSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.promotedPerRealSec, Unit.MPS);
        ratesUnitMap.put(RateEnum.promotedPerTotalSec, Unit.MPS);

        return ratesUnitMap;
    }

    private Map<GCLogDataEnum, Unit> buildStatsUnitMap() {
        Map<GCLogDataEnum, Unit> statsUnitMap = new HashMap<>();

        statsUnitMap.put(GCLogDataEnum.younggenmemorycleared, Unit.M);
        statsUnitMap.put(GCLogDataEnum.frequencies, Unit.FREQ);
        statsUnitMap.put(GCLogDataEnum.heapoccupanciesbeforegc, Unit.M);
        statsUnitMap.put(GCLogDataEnum.cpuusertimes, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.younggenoccupanciesaftergc, Unit.M);
        statsUnitMap.put(GCLogDataEnum.cpusystimes, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.durations, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.timesbetweengarbagecollections, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.heapoccupanciesaftergc, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.memoryallocated, Unit.M);
        statsUnitMap.put(GCLogDataEnum.oldgenaftergc, Unit.M);
        statsUnitMap.put(GCLogDataEnum.timestamps, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.memorypromoted, Unit.M);
        statsUnitMap.put(GCLogDataEnum.oldgenbeforegc, Unit.M);
        statsUnitMap.put(GCLogDataEnum.realtimes, Unit.SEC);
        statsUnitMap.put(GCLogDataEnum.younggenoccupanciesbeforegc, Unit.M);

        return statsUnitMap;
    }
}
