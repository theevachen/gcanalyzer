package com.yammer.coreservices.gc;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by echen on 8/12/14.
 */
public class OptionsParser {
    ArgumentParser parser;
    Namespace ns;
    List<GCLogDataEnum> listOfEnums;

    public OptionsParser() {
        this.parser = ArgumentParsers.newArgumentParser("Program")
                .description("Print GC log stats and data.");
        this.ns = null;

        this.listOfEnums = new ArrayList<>();
    }

    public ArgumentParser getParser() {
        return parser;
    }

    public Namespace getNs() {
        return ns;
    }

    public List<GCLogDataEnum> getListOfEnums() {
        return listOfEnums;
    }

    public Options parseOptions(String[] args) {

        addArgument("-timestamps", false);
        addArgument("-timesbetweengarbagecollections", false);
        addArgument("-durations", false);
        addArgument("-frequencies", false);
        addArgument("-younggenoccupanciesbeforegc", false);
        addArgument("-younggenoccupanciesaftergc", false);
        addArgument("-heapoccupanciesbeforegc", false);
        addArgument("-heapoccupanciesaftergc", false);
        addArgument("-memoryallocated", false);
        addArgument("-younggenmemorycleared", false);
        addArgument("-memorypromoted", false);
        addArgument("-oldgenbeforegc", false);
        addArgument("-oldgenaftergc", false);
        addArgument("cpuusertimes", false);
        addArgument("-cpusystimes", false);
        addArgument("-realtimes", false);
        addArgument("-summary", true);
        parser.addArgument("file");

        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        for (GCLogDataEnum e : GCLogDataEnum.values()) {
            if (ns.getBoolean(e.name())) {
                listOfEnums.add(e);
            }
        }
        Options options = new Options(ns.getBoolean("summary"), listOfEnums, ns.getString("file"));

        return options;
    }

    public void addArgument(String argumentName, Boolean defaultBool) {
        parser.addArgument(argumentName)
                .type(Boolean.class)
                .setDefault(defaultBool);
    }
}
