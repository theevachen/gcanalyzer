package com.yammer.coreservices.gc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by echen on 8/12/14.
 */
public class DataEvent {

    public static final int GROUP_FOR_TIME_STAMPS = 1;
    public static final int YOUNG_GEN_OCCUPANCY_BEFORE_GC = 3;
    public static final int YOUNG_GEN_OCCUPANCY_AFTER_GC = 5;
    public static final int GROUP_FOR_DURATION = 9;
    public static final int HEAP_OCCUPANCY_BEFORE_GC = 10;
    public static final int HEAP_OCCUPANCY_AFTER_GC = 12;
    public static final int CPU_USER_TIME = 17;
    public static final int CPU_SYS_TIME = 18;
    public static final int REAL_TIME = 19;

    private List<ParNewEvent> parNewEvents;

    public DataEvent(String file) throws IOException {
        this(new BufferedReader(new FileReader(file)));
    }

    public DataEvent(BufferedReader reader) throws IOException {
        String parNewPattern = "([0-9]+.[0-9]+): \\[(ParNew): ([0-9]+)([KM])->([0-9]+)([KM])\\(([0-9]+)([KM])\\). ([0-9]+.[0-9]+) secs\\] ([0-9]+)([KM])->([0-9]+)([KM])\\(([0-9]+)([KM])\\)\\, ([0-9]+.[0-9]+) secs\\] \\[Times: user=([0-9]+.[0-9]+) sys=([0-9]+.[0-9]+). real=([0-9]+.[0-9]+) secs\\]";
//        String cmsInitialMarkPattern = "([0-9]+.[0-9]+): \\[GC \\[1 (AS)?CMS-initial-mark: ([0-9]+)K\\(([0-9]+)K\\)\\] ([0-9]+)K\\(([0-9]+)K\\), ([0-9][0-9.]*) secs\\] \\[Times: user=([0-9]+.[0-9]+) sys=([0-9]+.[0-9]+), real=([0-9]+.[0-9]+) secs\\]";
        String line;

        Pattern compiledParNewPattern = Pattern.compile(parNewPattern);
//        Pattern compiledCmsPattern = Pattern.compile(cmsInitialMarkPattern);

        parNewEvents = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            Matcher parNewMatcher = compiledParNewPattern.matcher(line);
//            Matcher cmsInitialMarkMatcher = compiledCmsPattern.matcher(line);
            if (parNewMatcher.find()) {
                Map<GCLogDataEnum, Double> map = new HashMap<>();
                map.put(GCLogDataEnum.timestamps, Double.parseDouble(parNewMatcher.group(GROUP_FOR_TIME_STAMPS)));
                map.put(GCLogDataEnum.durations, Double.parseDouble(parNewMatcher.group(GROUP_FOR_DURATION)));
                map.put(GCLogDataEnum.younggenoccupanciesbeforegc, Double.parseDouble(parNewMatcher.group(YOUNG_GEN_OCCUPANCY_BEFORE_GC)) / 1000.0);
                map.put(GCLogDataEnum.younggenoccupanciesaftergc, Double.parseDouble(parNewMatcher.group(YOUNG_GEN_OCCUPANCY_AFTER_GC)) / 1000.0);
                map.put(GCLogDataEnum.heapoccupanciesbeforegc, Double.parseDouble(parNewMatcher.group(HEAP_OCCUPANCY_BEFORE_GC)) / 1000.0);
                map.put(GCLogDataEnum.heapoccupanciesaftergc, Double.parseDouble(parNewMatcher.group(HEAP_OCCUPANCY_AFTER_GC)) / 1000.0);
                map.put(GCLogDataEnum.cpuusertimes, Double.parseDouble(parNewMatcher.group(CPU_USER_TIME)));
                map.put(GCLogDataEnum.cpusystimes, Double.parseDouble(parNewMatcher.group(CPU_SYS_TIME)));
                map.put(GCLogDataEnum.realtimes, Double.parseDouble(parNewMatcher.group(REAL_TIME)));
                ParNewEvent parNewEvent = new ParNewEvent(map);
                parNewEvents.add(parNewEvent);
            }
        }
        if(parNewEvents.isEmpty()){
            throw new IllegalArgumentException("File Contained No Events");
        }
        generateData();
    }

    public Map<GCLogDataEnum, Statistic> generateStatistics() {
        Map<GCLogDataEnum, Statistic> statistics = new HashMap<>();
        StatisticCalculator calculator = new StatisticCalculator();
        for (GCLogDataEnum e : GCLogDataEnum.values()) {
            List<Double> data = new ArrayList<>();
            for (ParNewEvent parNewEvent : parNewEvents) {
                if (parNewEvent.map.get(e) != null) {
                    data.add(parNewEvent.map.get(e));
                }
            }
            statistics.put(e, calculator.calculateAll(data));
        }

        return statistics;
    }

    public List<ParNewEvent> getParNewEvents() {
        return parNewEvents;
    }

    public ParNewEvent get(int i) {
        return parNewEvents.get(i);
    }

    private void generateData() {
        for (int i = 1; i < parNewEvents.size(); i++) {
            // the times between each garbage collection
            parNewEvents.get(i - 1).map.put(GCLogDataEnum.timesbetweengarbagecollections, parNewEvents.get(i).map.get(GCLogDataEnum.timestamps) - parNewEvents.get(i - 1).map.get(GCLogDataEnum.timestamps));
            // the amount of young generation memory allocated from after a gc to before another starts
            parNewEvents.get(i - 1).map.put(GCLogDataEnum.memoryallocated, parNewEvents.get(i).map.get(GCLogDataEnum.younggenoccupanciesbeforegc) - parNewEvents.get(i - 1).map.get(GCLogDataEnum.younggenoccupanciesaftergc));
        }

        for (int i = 0; i < parNewEvents.size() - 1; i++) {
            // the inverse of times between each garbage collection. so frequency
            parNewEvents.get(i).map.put(GCLogDataEnum.frequencies, 1 / parNewEvents.get(i).map.get(GCLogDataEnum.timesbetweengarbagecollections));
        }

        for (int i = 0; i < parNewEvents.size(); i++) {
            // the amount of memory cleared after a garbage collection in young generation
            parNewEvents.get(i).map.put(GCLogDataEnum.younggenmemorycleared, parNewEvents.get(i).map.get(GCLogDataEnum.younggenoccupanciesbeforegc) - parNewEvents.get(i).map.get(GCLogDataEnum.younggenoccupanciesaftergc));
            // the old gen occupancy before garbage collection
            parNewEvents.get(i).map.put(GCLogDataEnum.oldgenbeforegc, parNewEvents.get(i).map.get(GCLogDataEnum.heapoccupanciesbeforegc) - parNewEvents.get(i).map.get(GCLogDataEnum.younggenoccupanciesbeforegc));
            // the old gen occupancy after garbage collection
            parNewEvents.get(i).map.put(GCLogDataEnum.oldgenaftergc, parNewEvents.get(i).map.get(GCLogDataEnum.heapoccupanciesaftergc) - parNewEvents.get(i).map.get(GCLogDataEnum.younggenoccupanciesaftergc));
            // memory promoted from young gen to old gen
            parNewEvents.get(i).map.put(GCLogDataEnum.memorypromoted, parNewEvents.get(i).map.get(GCLogDataEnum.oldgenaftergc) - parNewEvents.get(i).map.get(GCLogDataEnum.oldgenbeforegc));
        }
    }

    public Rate generateRate(Map<GCLogDataEnum, Statistic> statistics) {
        Rate rate = new Rate();

        rate.getMap().put(RateEnum.allocPerUserSec, (statistics.get(GCLogDataEnum.memoryallocated).getTotal() / 1000) / statistics.get(GCLogDataEnum.cpuusertimes).getTotal());
        rate.getMap().put(RateEnum.allocPerSysSec, (statistics.get(GCLogDataEnum.memoryallocated).getTotal() / 1000) / statistics.get(GCLogDataEnum.cpusystimes).getTotal());
        rate.getMap().put(RateEnum.allocPerRealSec, (statistics.get(GCLogDataEnum.memoryallocated).getTotal() / 1000) / statistics.get(GCLogDataEnum.realtimes).getTotal());
        rate.getMap().put(RateEnum.allocPerTotalSec, (statistics.get(GCLogDataEnum.memoryallocated).getTotal() / 1000) / parNewEvents.get(parNewEvents.size() - 1).map.get(GCLogDataEnum.timestamps));

        rate.getMap().put(RateEnum.promotedPerUserSec, (statistics.get(GCLogDataEnum.memorypromoted).getTotal() / 1000) / statistics.get(GCLogDataEnum.cpuusertimes).getTotal());
        rate.getMap().put(RateEnum.promotedPerSysSec, (statistics.get(GCLogDataEnum.memorypromoted).getTotal() / 1000) / statistics.get(GCLogDataEnum.cpusystimes).getTotal());
        rate.getMap().put(RateEnum.promotedPerRealSec, (statistics.get(GCLogDataEnum.memorypromoted).getTotal() / 1000) / statistics.get(GCLogDataEnum.realtimes).getTotal());
        rate.getMap().put(RateEnum.promotedPerTotalSec, (statistics.get(GCLogDataEnum.memorypromoted).getTotal() / 1000) / parNewEvents.get(parNewEvents.size() - 1).map.get(GCLogDataEnum.timestamps));

        return rate;
    }
}
