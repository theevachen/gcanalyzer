package com.yammer.coreservices.gc;

/**
 * Created by echen on 8/28/14.
 */
public enum RateEnum {
    allocPerUserSec,
    allocPerSysSec,
    allocPerRealSec,
    allocPerTotalSec,
    promotedPerUserSec,
    promotedPerSysSec,
    promotedPerRealSec,
    promotedPerTotalSec;
}
