package com.yammer.coreservices.gc;

import java.util.List;

/**
 * Created by echen on 8/12/14.
 */
public class Options {
    String fileName;
    List<GCLogDataEnum> enums;
    Boolean summary;

    public Options(Boolean summary, List<GCLogDataEnum> enums, String fileName) {
        this.summary = summary;
        this.enums = enums;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public List<GCLogDataEnum> getEnums() {
        return enums;
    }

    public Boolean getSummary() {
        return summary;
    }

}
