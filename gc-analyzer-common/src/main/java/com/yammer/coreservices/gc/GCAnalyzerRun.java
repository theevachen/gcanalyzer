package com.yammer.coreservices.gc;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by echen on 8/29/14.
 */
public class GCAnalyzerRun {

    List<StatisticData> statisticDataList;
    Map<RateEnum, Double> rateMap;

    public GCAnalyzerRun(DataEvent dataEvent, UnitProvider unitProvider, Rate rate) {
        statisticDataList = new ArrayList<>();
        rateMap = new HashMap<>();
        for (GCLogDataEnum e : GCLogDataEnum.values()) {
            StatisticData statisticData = new StatisticData(dataEvent, e, unitProvider);
            statisticDataList.add(statisticData);
        }
        for (RateEnum e : RateEnum.values()) {
            rateMap = rate.getMap();
        }
    }

    public GCAnalyzerRun(@JsonProperty("statisticDataList")List<StatisticData> list, @JsonProperty("rateMap") Map<RateEnum, Double> rateMap){
        this.statisticDataList = list;
        this.rateMap = rateMap;
    }

    public List<StatisticData> getStatisticDataList() {
        return statisticDataList;
    }

    public Map<RateEnum, Double> getRateMap() {
        return rateMap;
    }
}
