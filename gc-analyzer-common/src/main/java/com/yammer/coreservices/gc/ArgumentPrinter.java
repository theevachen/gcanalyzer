package com.yammer.coreservices.gc;

import net.sourceforge.argparse4j.inf.Namespace;

import java.util.List;

/**
 * Created by echen on 8/14/14.
 */
public class ArgumentPrinter {

    public void printArgument(GCLogDataEnum enums, DataEvent data, Namespace ns) {
        List<ParNewEvent> events = data.getParNewEvents();

        if (ns.getBoolean(enums.name())) {
            for (ParNewEvent parNewEvent : events) {
                System.out.println(parNewEvent.map.get(enums));
            }
        }
    }
}
