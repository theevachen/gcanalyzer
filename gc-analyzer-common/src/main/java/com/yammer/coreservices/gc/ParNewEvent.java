package com.yammer.coreservices.gc;

import java.util.Map;

/**
 * Created by echen on 8/8/14.
 */
public class ParNewEvent {

    Map<GCLogDataEnum, Double> map;

    public ParNewEvent(Map<GCLogDataEnum, Double> map) {
        this.map = map;
    }

    public Map<GCLogDataEnum, Double> getMap() {
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParNewEvent that = (ParNewEvent) o;

        if (map != null ? !map.equals(that.map) : that.map != null) return false;

        return true;
    }
}
