package com.yammer.coreservices.gc;

import java.util.Collections;
import java.util.List;

/**
 * Created by echen on 7/22/14.
 */
public class StatisticCalculator {

    public static final double FIFTIETH_PERCENTILE = .5;
    public static final double NINETIETH_PERCENTILE = .9;
    public static final double NINETY_FIFTH_PERCENTILE = .95;
    public static final double NINETY_NINTH_PERCENTILE = .99;
    public static final double NINETY_NINE_POINT_NINTH_PERCENTILE = .999;

    public double calculateTotal(List<Double> list) {
        double total = 0;

        for (int i = 0; i < list.size(); i++) {
            total += list.get(i);
        }

        return total;
    }

    public double calculateAverage(List<Double> list) {

        return calculateTotal(list) / list.size();
    }

    public double calculateMax(List<Double> list) {
        double max = 0;

        for (Double value : list) {
            max = Math.max(max, value);
        }

        return max;
    }

    public double calculateMin(List<Double> list) {
        double min = list.get(0);

        for (Double value : list) {
            min = Math.min(min, value);
        }

        return min;
    }

    public double calculateMedian(List<Double> list) {
        Collections.sort(list);
        long index = Math.round(FIFTIETH_PERCENTILE * list.size()) - 1;

        return list.get((int) index);
    }

    public double calculateNinetiethPercentile(List<Double> list) {
        Collections.sort(list);
        long index = Math.round(NINETIETH_PERCENTILE * list.size()) - 1;

        return list.get((int) index);
    }

    public double calculateNinetyFifthPercentile(List<Double> list) {
        Collections.sort(list);
        long index = Math.round(NINETY_FIFTH_PERCENTILE * list.size()) - 1;

        return list.get((int) index);
    }

    public double calculateNinetyNinthPercentile(List<Double> list) {
        Collections.sort(list);
        long index = Math.round(NINETY_NINTH_PERCENTILE * list.size()) - 1;

        return list.get((int) index);
    }

    public double calculateNinetyNinePointNinthPercentile(List<Double> list) {
        Collections.sort(list);
        long index = Math.round(NINETY_NINE_POINT_NINTH_PERCENTILE * list.size()) - 1;

        return list.get((int) index);
    }

    public Statistic calculateAll(List<Double> list) {
        double total = calculateTotal(list);
        double average = calculateAverage(list);
        double max = calculateMax(list);
        double min = calculateMin(list);
        double median = calculateMedian(list);
        double ninetiethPercentile = calculateNinetiethPercentile(list);
        double ninetyFifthPercentile = calculateNinetyFifthPercentile(list);
        double ninetyNinthPercentile = calculateNinetyNinthPercentile(list);
        double ninetyNinePointNinthPercentile = calculateNinetyNinePointNinthPercentile(list);

        return new Statistic(average, max, min, median, ninetiethPercentile, ninetyFifthPercentile, ninetyNinthPercentile, ninetyNinePointNinthPercentile, total);

    }
}