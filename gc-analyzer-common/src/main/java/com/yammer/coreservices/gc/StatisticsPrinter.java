package com.yammer.coreservices.gc;

/**
 * Created by echen on 8/14/14.
 */
public class StatisticsPrinter {

    public void printStatistics(GCLogDataEnum enums, Statistic stats, String format, UnitProvider unitProvider) {

        double total = stats.getTotal();
        double average = stats.getAverage();
        double max = stats.getMax();
        double min = stats.getMin();
        double median = stats.getMedian();
        double ninetiethPercentile = stats.getNinetiethPercentile();
        double ninetyFifthPercentile = stats.getNinetyFifthPercentile();
        double ninetyNinthPercentile = stats.getNinetyNinthPercentile();
        double ninetyNinePointNinthPercentile = stats.getNinetyNinePointNinthPercentile();

        String leftAlignFormat = format;
        System.out.format(leftAlignFormat, enums.name(), unitProvider.getStatsUnitMap().get(enums), total, average, max, min, median, ninetiethPercentile, ninetyFifthPercentile, ninetyNinthPercentile, ninetyNinePointNinthPercentile);

    }
}
