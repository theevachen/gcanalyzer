package com.yammer.coreservices.gc;

import java.util.Map;

/**
 * Created by echen on 8/14/14.
 */
public class Printer {

    StatisticsPrinter statsPrinter;
    ArgumentPrinter argumentPrinter;
    RatePrinter ratePrinter;
    UnitProvider unitProvider;
    String statsFormat = "%-32s%-9s%-18.3f%-15.3f%-15.3f%-15.3f%-15.3f%-15.3f%-15.3f%-15.3f%-15.3f%n";
    String statsTitleFormat = "%-32s%-9s%-18s%-15s%-15s%-15s%-15s%-15s%-15s%-15s%-15s%n";
    String rateFormat = "%-32s%-15s%-15f%n";
    String rateTitleFormat = "%-32s%-15s%-15s%n";

    public Printer() {
        this.statsPrinter = new StatisticsPrinter();
        this.argumentPrinter = new ArgumentPrinter();
        this.ratePrinter = new RatePrinter();
        this.unitProvider = new UnitProvider();
    }

    public void print(Options options, OptionsParser parser, Map<GCLogDataEnum, Statistic> statistics, DataEvent data, Rate rate) {
        if (options.summary) {
            System.out.format(statsTitleFormat, "Data", "Units", "Total", "Avg", "Max", "Min", "Median", "90th", "95th", "99th", "99.9th");
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            for (Map.Entry<GCLogDataEnum, Statistic> entry : statistics.entrySet()) {
                statsPrinter.printStatistics(entry.getKey(), entry.getValue(), statsFormat, unitProvider);
            }
            System.out.println();
            System.out.format(rateTitleFormat, "Rate Type", "Units", "Rate");
            System.out.println("------------------------------------------------------------");
            for (RateEnum rateEnum : RateEnum.values()) {
                ratePrinter.printRate(rateEnum, rate, rateFormat, unitProvider);
            }
        } else {
            for (GCLogDataEnum e : options.enums) {
                System.out.println(e);
                argumentPrinter.printArgument(e, data, parser.ns);
                System.out.println();
            }
        }
    }
}
