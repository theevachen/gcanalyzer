package com.yammer.coreservices.gc;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by echen on 8/29/14.
 */
public class StatisticData {
    String data;
    String unit;
    Statistic statistic;

    public StatisticData(DataEvent reader, GCLogDataEnum enums, UnitProvider unitProvider) {
        this.data = enums.name();
        this.unit = unitProvider.getStatsUnitMap().get(enums).toString();
        this.statistic = reader.generateStatistics().get(enums);
    }

    public StatisticData(@JsonProperty("data") String data,
                         @JsonProperty("unit") String unit,
                         @JsonProperty("statistic") Statistic statistic){
        this.data = data;
        this.unit = unit;
        this.statistic = statistic;
    }

    public String getData() {
        return data;
    }

    public String getUnit() {
        return unit;
    }

    public Statistic getStatistic() {
        return statistic;
    }
}
