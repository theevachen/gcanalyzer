package com.yammer.coreservices.gc;

/**
 * Created by echen on 8/12/14.
 */
public enum GCLogDataEnum {
    timestamps,
    timesbetweengarbagecollections,
    durations,
    frequencies,
    younggenoccupanciesbeforegc,
    younggenoccupanciesaftergc,
    heapoccupanciesbeforegc,
    heapoccupanciesaftergc,
    memoryallocated,
    younggenmemorycleared,
    memorypromoted,
    oldgenbeforegc,
    oldgenaftergc,
    cpuusertimes,
    cpusystimes,
    realtimes;
}
