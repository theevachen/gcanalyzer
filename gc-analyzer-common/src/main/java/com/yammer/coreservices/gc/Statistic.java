package com.yammer.coreservices.gc;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by echen on 7/22/14.
 */
public class Statistic {

    double average;
    double max;
    double min;
    double median;
    double ninetiethPercentile;
    double ninetyFifthPercentile;
    double ninetyNinthPercentile;
    double ninetyNinePointNinthPercentile;
    double total;

    public Statistic(@JsonProperty("average") double average,
                     @JsonProperty("max") double max,
                     @JsonProperty("min") double min,
                     @JsonProperty("median") double median,
                     @JsonProperty("ninetiethPercentile") double ninetiethPercentile,
                     @JsonProperty("ninetyFifthPercentile") double ninetyFifthPercentile,
                     @JsonProperty("ninetyNinthPercentile") double ninetyNinthPercentile,
                     @JsonProperty("ninetyNinePointNinthPercentile") double ninetyNinePointNinthPercentile,
                     @JsonProperty("total") double total) {
        this.average = average;
        this.max = max;
        this.min = min;
        this.median = median;
        this.ninetiethPercentile = ninetiethPercentile;
        this.ninetyFifthPercentile = ninetyFifthPercentile;
        this.ninetyNinthPercentile = ninetyNinthPercentile;
        this.ninetyNinePointNinthPercentile = ninetyNinePointNinthPercentile;
        this.total = total;
    }

    public double getAverage() {
        return average;
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public double getMedian() {
        return median;
    }

    public double getNinetiethPercentile() {
        return ninetiethPercentile;
    }

    public double getNinetyFifthPercentile() {
        return ninetyFifthPercentile;
    }

    public double getNinetyNinthPercentile() {
        return ninetyNinthPercentile;
    }

    public double getNinetyNinePointNinthPercentile() {
        return ninetyNinePointNinthPercentile;
    }

    public double getTotal() {
        return total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Statistic statistic = (Statistic) o;

        if (Math.abs(statistic.average - average) > .0001) return false;
        if (Math.abs(statistic.max - max) > .0001) return false;
        if (Math.abs(statistic.median - median) > .0001) return false;
        if (Math.abs(statistic.min - min) > .0001) return false;
        if (Math.abs(statistic.ninetiethPercentile - ninetiethPercentile) > .0001) return false;
        if (Math.abs(statistic.ninetyFifthPercentile - ninetyFifthPercentile) > .0001) return false;
        if (Math.abs(statistic.ninetyNinePointNinthPercentile - ninetyNinePointNinthPercentile) > .0001) return false;
        if (Math.abs(statistic.ninetyNinthPercentile - ninetyNinthPercentile) > .0001) return false;
        if (Math.abs(statistic.total - total) > .0001) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(average);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(max);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(min);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(median);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ninetiethPercentile);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ninetyFifthPercentile);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ninetyNinthPercentile);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ninetyNinePointNinthPercentile);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(total);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}