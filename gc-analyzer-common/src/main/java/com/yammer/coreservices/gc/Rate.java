package com.yammer.coreservices.gc;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by echen on 8/28/14.
 */
public class Rate {

    Map<RateEnum, Double> map;

    public Rate() {
        this.map = new HashMap<>();
    }

    public Map<RateEnum, Double> getMap() {
        return map;
    }
}
