package com.yammer.coreservices.gc;

/**
 * Created by echen on 7/24/14.
 */
public enum Unit {

    M("MB"), FREQ("GC/sec"), SEC("sec"), MPS("MB/sec");

    private String unit;

    Unit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return unit;
    }
}
