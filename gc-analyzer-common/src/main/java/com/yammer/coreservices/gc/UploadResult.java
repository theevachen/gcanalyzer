package com.yammer.coreservices.gc;

/**
 * Created by echen on 9/2/14.
 */
public class UploadResult {
    private String uuid;

    public UploadResult(String uuid) {
        this.uuid = uuid;
    }

    public String getId() {
        return uuid;
    }
}
