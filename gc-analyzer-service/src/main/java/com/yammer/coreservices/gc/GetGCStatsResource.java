package com.yammer.coreservices.gc;


import com.basho.riak.client.IRiakClient;
import com.basho.riak.client.RiakException;
import com.basho.riak.client.RiakFactory;
import com.basho.riak.client.bucket.Bucket;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.Map;
import java.util.UUID;

/**
 * Created by echen on 8/29/14.
 */
@Path("/stats")
public class GetGCStatsResource {

    String uuid = "";
    IRiakClient riakClient;
    Bucket myBucket;

    public GetGCStatsResource() throws RiakException{
        this.riakClient = RiakFactory.httpClient();
        this.myBucket = riakClient.fetchBucket("gc-analyzer-test").execute();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public UploadResult uploadFile(@FormDataParam("file") InputStream uploadedInputStream) throws RiakException{

        DataEvent dataEvent;
        BufferedReader reader = new BufferedReader(new InputStreamReader(uploadedInputStream));
        try {
            dataEvent = new DataEvent(reader);
        }
        catch(IOException | IllegalArgumentException e ){
            throw new WebApplicationException(e, Response.status(Response.Status.BAD_REQUEST).entity("ERROR").build());
        }
        Map<GCLogDataEnum, Statistic> statisticMap = dataEvent.generateStatistics();
        Rate rate = dataEvent.generateRate(statisticMap);
        UnitProvider unitProvider = new UnitProvider();

        uuid = UUID.randomUUID().toString();

        GCAnalyzerRun gcAnalyzerRun = new GCAnalyzerRun(dataEvent, unitProvider, rate);

        myBucket.store(uuid, gcAnalyzerRun).execute();

        UploadResult uploadResult = new UploadResult(uuid);
        return uploadResult;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public GCAnalyzerRun getResults(@QueryParam("uuid") String uuid) throws IOException, RiakException {

        GCAnalyzerRun gcAnalyzerRun = myBucket.fetch(uuid, GCAnalyzerRun.class).execute();

        return gcAnalyzerRun;
    }


}
