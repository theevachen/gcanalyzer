package com.yammer.coreservices.gc;


import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Created by echen on 8/25/14.
 */
public class GCAnalyzerService extends Application<GCAnalyzerConfiguration> {
    public static void main(String[] args) throws Exception {
        new GCAnalyzerService().run(args);
    }

    @Override
    public void initialize(Bootstrap<GCAnalyzerConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets", "/", "index.html"));
    }

    @Override
    public void run(GCAnalyzerConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern("/api/*");
        environment.jersey().register(new GetGCStatsResource());
        environment.jersey().register(new HelloResource());
    }
}
