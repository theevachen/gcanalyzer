'use strict';

var GcApp = angular.module('GcApp', ['ngRoute', 'angularFileUpload']);

GcApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: 'partials/service.html' ,
        controller: 'IndexController'
    });

    $routeProvider.when('/table/:uuid', {
        templateUrl: 'partials/table.html' ,
        controller: 'TableController'
    });

    $routeProvider.otherwise({redirectTo: '/'});

}]);