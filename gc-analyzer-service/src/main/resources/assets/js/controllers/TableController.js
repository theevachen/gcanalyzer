
GcApp.controller("TableController", function($scope, $http, $filter, $upload, $routeParams, $location) {

    $scope.gcAnalyzerRun = [];

    $http.get('/api/stats?uuid=' + $routeParams.uuid).success(function (gcAnalyzerRun) {
            $scope.gcAnalyzerRun = gcAnalyzerRun;
    });

    $http.get('/partials/table.html').success(function (gcAnalyzerRun){
            $scope.gcAnalyzerRun = gcAnalyzerRun;
    });

    $scope.numberFilter = function($filter){
        return $filter('number')(number, fractionSize);
    }
});
