package com.yammer.coreservices.gc;

import java.io.IOException;
import java.util.Map;

/**
 * Main interface for the CLI.
 */
public class CommandLineInterface {
    public static void main(String[] args) throws IOException {
        OptionsParser optionsParser = new OptionsParser();
        Printer printer = new Printer();

        Options options = optionsParser.parseOptions(args);
        DataEvent dataEvent = new DataEvent(options.fileName);

        Map<GCLogDataEnum, Statistic> statisticMap = dataEvent.generateStatistics();
        Rate rate = dataEvent.generateRate(statisticMap);

        printer.print(options, optionsParser, statisticMap, dataEvent, rate);
    }
}
