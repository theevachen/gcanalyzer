#How to run gc-analyzer
There are two ways to use this tool. You can either run it from the command line, or start up the service locally and use it from the web.

##Setup
Run the below code from terminal.
```sh
git clone https://github.int.yammer.com/yammer/gc-analyzer.git
cd gc-analyzer
mvn clean package
```

##Running from the command line
The following command will execute the program in its default settings.
```sh
./gcAnalyzer file TestInput.log
```
The data will be displayed in a table as shown below.

![Summary](Images/summary_example.jpg)

The data that is returned can be changed in the command line. Command line arguments are as follows...

| Argument               | Default |
| -----------------------| --------|
| -summary               | true    |
| -timestamps            | false   |
| -timesbetweengarbagecollections | false |
| -durations | false |
| -frequencies | false |
| -younggenoccupanciesbeforegc | false |
| -younggenoccupanciesaftergc | false |
| -heapoccupanciesbeforegc | false |
| -heapoccupanciesaftergc | false |
| -heapoccupanciesbeforegc | false |
| -memoryallocated | false |
| -younggenmemorycleared | false |
| -memorypromoted | false |
| -oldgenbeforegc | false |
| -cpuusertimes | false |
| -cpusystimes | false |
| -realtimes | false |

The different arguments can be set as shown in the following example.
```sh
./gcAnalyzer.sh -summary false -timestamps true -memoryallocated true file TestInput.log
```
This example has the timestamps argument and memory allocated argument set to true and will print out a list of all the timestamps. If you set it to one argument at a time, this data can be piped to a excel file and such, to then be displayed in graphs or some kind of visual display.

<img src=Images/arguments_example.png width=100>

For the memoryallocated argument, and a couple other arguments, there will a null at the end. There is nothing wrong with the data. It is just generating a null because arguments like memoryallocated are being dervied from taking the difference of values from two events, while I am looping through every single event to print the values.
##Running as a service
```sh
cd gc-analyzer-service
java -jar gc-analyzer-service/target/gc-analyzer-service-0.0.1-SNAPSHOT.jar server
```
Open up the port 8080 on your local host. You should arrive at a page that allows you to upload a file. Note: Only files with a .log extension can be submitted.

<img src=Images/file_input.png width=350>

Note: At this time, only log files in a certain format can be parsed.

After you submit a file, it should automatically redirect you to the table page that has all the values for your file already filled in. If the format is not correct, then it will return a Bad Request Error (400). I currently do not have it displaying an eror outside of the console page, and it is something that can be added.

<img src=Images/table.png width=700>

After, you can then click on the Garbage Collection Analyzer button at the top and you can then submit another file and the table will automatically populate with the new data. At any time, you can save the link or send it to someone else, and it show the same information as it did when you first submitted the file that populated it.
