#Garbage Collection Analyzer

A tool that helps analyze garbage collection processes by returning statistical data derived from garbage collection log files.

Provides data that may be useful in tuning garbage collection.

###Ways to run this tool
- Command Line
- Service (Web)

###Features not yet supported
- Only CMS is supported at this time.
- Only log files in a certain format are currently supported.

 Example of working log file:
![Log File](Images/log_format.png)

- The service can only display the summary. It can't return the values for specific arguments. Only the command line supports this feature right now.
